var app = angular.module('mainApp',[]);

app.controller('apiController', function($scope, $http, $location){
	$scope.displayElements ={
		strings:{
			auth:false,
			post:true	
		},
		posts:{
			auth: false,
			query: false,
		},
		responseFormat:"raw"		
	}
	$scope.allowedMethods = [
		"getLeads",
		"getCases",
		"getAccounts",
		"getDeals",
		"getContacts",
		"getEmployees",
		"getImpressions",
		"getEmailData"
	]
	$scope.allowedFilterOperators = [
		"=",
		"<",
		">",
		"!=",
		">=",
		"<="
	]
	$scope.auth = {
		subdomain:'dwhite',
		username:'admin',
		password:'admin',
		token:'GA54KMH02C1RuPBBfdpmKgfQckLUQ4o2'
	};
	$scope.responseObject = 1;
	$scope.serverResponse={
		status:'',
		data:''
	}
	$scope.filters = [
		{field:"first_name", operator:"=", values:['Frodo']}
	];
	$scope.apiMethod = "getLeads";
	$scope.postString = {
		"operation":$scope.apiMethod,
		"parameters":[
			$scope.filters
			]
	};
	$scope.authString = {
		"operation":"apiLogin",
		"parameters":[
			$scope.auth.username,
			$scope.auth.password,
			$scope.auth.token
		]
	};

	$scope.removeFilter = function(item){
		$scope.filters.splice(item,1);
	}
	$scope.addFilter = function(){
		var newFilter = {field:"", operator:"", values:['']};
		$scope.filters.push(newFilter);
	}
	$scope.updatePostString = function(){
		$scope.postString = {
				"operation":$scope.apiMethod,
				"parameters":[
					$scope.filters,
					$scope.offset,
					$scope.limit
				]
		};
	}
	$scope.updateAuthString = function(){
		$scope.authString = {
			"operation":"apiLogin",
			"parameters":[
				$scope.auth.username,
				$scope.auth.password,
				$scope.auth.token
			]
		};
	}
	$scope.authenticate = function(){
		$http.post('http://'+$location.host()+':'+$location.port()+'/auth',$scope.auth)
		.then(function(response){
			$scope.serverResponse.status = response.status;
			$scope.serverResponse.data = response.data;
		})
	}
	$scope.queryAPI = function(){
		$http.post('http://'+$location.host()+':'+$location.port()+'/apiQuery', {"subdomain":$scope.auth.subdomain,"post":$scope.postString})
		.then(function(response){
			$scope.serverResponse.status = response.status;
			$scope.serverResponse.data = response.data;
		})
	}
})