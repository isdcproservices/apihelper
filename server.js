var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');

var app = express();

var sessionID;

app.use(express.static(__dirname+'/'));
app.use(bodyParser.json());

//start server
var port = 3001;
app.listen(port, function(){
	console.log('Server is running at [IP Address]:'+port);
	console.log('To access main page, enter [IP Address]:'+port+'/home');
});

app.get('/home', function(req, res){
	res.sendFile(__dirname+'/index.html');
});

app.post('/apiQuery', function(req, res){
	res.writeHead(200,{'Content-Type':'text/plain'});
	api_request_JSON(req.body.subdomain, req.body.post, function(apiResponse){
		//console.log(apiResponse);
		res.end(JSON.stringify(apiResponse));
	});
});

app.post('/auth', function(req, res){
	console.log(req.body);
	res.writeHead(200,{'Content-Type':'text/plain'});
	authenticate(req.body, function(apiResponse){
		console.log('SessionID = '+sessionID);
		res.end(JSON.stringify(apiResponse));
	});
})

function api_request_JSON(subdomain, requestJSON, callback){
	var options = {
		method: "post",
		url:"https://"+subdomain+".insidesales.com/do=noauth/rest/service",
		type:"utf-8",
		json:true,
		headers:{
			Cookie : 'PHPSESSID='+sessionID
		},
		body: requestJSON
	}
	console.log(options);
	request(options, function(error, response, body){
		callback(body);
	});
}
function authenticate(authInfo, callback){
	var options = {
		method: "post",
		url:"https://"+authInfo.subdomain+".insidesales.com/do=noauth/rest/service",
		type:"utf-8",
		json:true,
		body:{
			operation:"apiLogin", 
			parameters:[authInfo.username, authInfo.password, authInfo.token]
		}
	}
	request(options, function(error, response, body){
		if(body.success){
			sessionID = body.access_token.split("|")[2];
		} else {
			sessionID = "invalid";
		}
		callback(body);
	});
}